# API仕様書 | SZPP 勉強会用 TODO アプリ

Web API の使用を記述する形式の1つに API Blueprint というものがあります (拡張子は `.apib` )。  
マークダウンを拡張した形式になっています。

ここのAPI仕様書は API Blueprint で記述されています。

`.apib` ファイルを解釈して HTML ファイルを生成するツールの1つに `aglio` というものがあります。


## 必要なツールのインストール

1. この README.md ファイルがあるディレクトリへ移動してください。

    ```
    cd path/to/this_README_directory
    ```

2. 筆者がインストールしたものと同じバージョンのパッケージ群をカレントディレクトリにインストールする。

    ```
    yarn
    ```

    ※ `yarn.lock` に書かれているパッケージ情報をもとにインストールする。  
    インストール後、`./node_modules/` 以下に aglio 等のパッケージがインストールされる。


## 仕様書の生成

```
yarn run build
```

このコマンドを実行すると、aglio が `./src/todo-api-spec.apib` をパースして `./aglio-out/todo-api-spec.html` にHTMLを書き出す。

あとはブラウザで開けばOK。

```
xdg-open ./aglio-out/todo-api-spec.html
```

※ `yarn run build` コマンドの定義は `./package.json` の `scripts` 内にあります。


## (おまけ) .apib を編集しながらリアルタイムでHTMLをプレビュー

1. サーバーを起動

    ```
    yarn run serve
    ```

2. 表示されたURLを開く (多分 localhost:3000)

3. `./src/todo-api-spec.apib` を編集して保存するとブラウザのHTML表示も自動で更新される

    `./aglio-out/` のほうには保存されないので注意 (あくまでプレビューするだけ)。
