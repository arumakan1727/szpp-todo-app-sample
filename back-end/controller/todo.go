package controller

import (
	"log"
	"net/http"
	"time"

	"github.com/arumakan1727/todo-app-pre/back-end/model"
	"github.com/arumakan1727/todo-app-pre/back-end/schema"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ginHandlerFunc = func(*gin.Context)

func RegisterTodoAPIs(r gin.IRouter, db *gorm.DB) {
	r.POST("/api/todo", PostNewTodo(db))
	r.GET("/api/todo", GetTodoList(db))
	r.PUT("/api/todo/:id", PutUpdatedTodo(db))
	r.PUT("/api/todo/:id/done", PutTodoDone(db))
	r.DELETE("/api/todo/:id", DeleteTodo(db))
}

func todo2resp(t *model.Todo) *schema.TodoCommonResp {
	return &schema.TodoCommonResp{
		ID:        t.ID,
		Subject:   t.Subject,
		Due:       t.MaybeDue(),
		Done:      t.Done,
		CreatedAt: t.CreatedAt,
		UpdatedAt: t.UpdatedAt,
	}
}

func PostNewTodo(db *gorm.DB) ginHandlerFunc {
	return func(c *gin.Context) {
		var reqJson schema.TodoCreateReq
		if err := c.BindJSON(&reqJson); err != nil {
			log.Println(err)
			return
		}

		newTodo := model.Todo{
			Subject: reqJson.Subject,
			Due:     model.ConvertDueIfNil(reqJson.Due),
			Done:    false,
		}
		res := db.Create(&newTodo)
		if res.Error != nil {
			log.Println(res.Error)
			log.Println(reqJson)
			log.Println(newTodo)
			c.String(http.StatusInternalServerError, "Failed to create new todo item")
			return
		}

		c.JSON(http.StatusCreated, todo2resp(&newTodo))
	}
}

func GetTodoList(db *gorm.DB) ginHandlerFunc {
	return func(c *gin.Context) {
		var todos []model.Todo
		db.Order("due asc").Order("created_at asc").Find(&todos)

		resp := make([]*schema.TodoCommonResp, 0, len(todos))
		for _, t := range todos {
			resp = append(resp, todo2resp(&t))
		}

		c.JSON(http.StatusOK, resp)
	}
}

func PutUpdatedTodo(db *gorm.DB) ginHandlerFunc {
	return func(c *gin.Context) {
		id, err := parseParamUint32(c, "id")
		if err != nil {
			log.Println(err)
			c.String(http.StatusBadRequest, "Invalid path param: "+c.Param("id"))
			return
		}

		var reqJson schema.TodoUpdateReq
		if err := c.BindJSON(&reqJson); err != nil {
			log.Println(err)
			return
		}

		var t model.Todo
		res := db.Take(&t, id)
		if res.Error != nil {
			log.Println(res.Error)
			c.String(http.StatusNotFound, "Item not found: `id`="+c.Param("id"))
			return
		}

		res = db.Model(&t).Updates(&model.Todo{
			Subject: reqJson.Subject,
			Due:     model.ConvertDueIfNil(reqJson.Due),
		})
		if res.Error != nil {
			log.Println(res.Error)
			log.Println(reqJson)
			c.String(http.StatusInternalServerError, "Failed to update item")
			return
		}

		c.JSON(http.StatusOK, todo2resp(&t))
	}
}

func PutTodoDone(db *gorm.DB) ginHandlerFunc {
	return func(c *gin.Context) {
		id, err := parseParamUint32(c, "id")
		if err != nil {
			log.Println(err)
			c.String(http.StatusBadRequest, "Invalid path param: "+c.Param("id"))
			return
		}

		var reqJson schema.TodoDoneUpdateReq
		if err := c.BindJSON(&reqJson); err != nil {
			log.Println(err)
			return
		}

		var t model.Todo
		res := db.Take(&t, id)
		if res.Error != nil {
			log.Println(res.Error)
			c.String(http.StatusNotFound, "Item not found: `id`="+c.Param("id"))
			return
		}

		res = db.Model(&t).Updates(&model.Todo{
			Done: reqJson.Done,
		})
		if res.Error != nil {
			log.Println(res.Error)
			log.Println(reqJson)
			c.String(http.StatusInternalServerError, "Failed to update item")
			return
		}

		c.JSON(http.StatusOK, todo2resp(&t))
	}
}

func DeleteTodo(db *gorm.DB) ginHandlerFunc {
	return func(c *gin.Context) {
		id, err := parseParamUint32(c, "id")
		if err != nil {
			log.Println(err)
			c.String(http.StatusBadRequest, "Invalid path param: "+c.Param("id"))
			return
		}

		tx := db.Begin()
		defer tx.Commit()
		now := time.Now()

		var t model.Todo
		res := tx.Take(&t, id)
		if res.Error != nil {
			log.Println(res.Error)
			c.String(http.StatusNotFound, "Item not found: `id`="+c.Param("id"))
			return
		}

		res = tx.Create(&model.DeletedTodo{
			Todo:      t,
			DeletedAt: now,
		})
		if res.Error != nil {
			log.Println(res.Error)
			c.String(http.StatusInternalServerError, "Failed to update item")
			return
		}

		res = db.Delete(&t)
		if res.Error != nil {
			log.Println(res.Error)
			c.String(http.StatusInternalServerError, "Failed to update item")
			return
		}

		c.String(http.StatusOK, "Deleted todo item successfully")
	}
}
