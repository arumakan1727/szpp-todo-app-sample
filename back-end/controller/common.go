package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

func parseParamUint32(c *gin.Context, key string) (uint32, error) {
	param := c.Param(key)
	n, err := strconv.ParseUint(param, 10, 32)
	if err != nil {
		return 0, err
	}
	return uint32(n), nil
}
