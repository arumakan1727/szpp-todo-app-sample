package controller_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/arumakan1727/todo-app-pre/back-end/controller"
	"github.com/arumakan1727/todo-app-pre/back-end/db"
	"github.com/arumakan1727/todo-app-pre/back-end/schema"
	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/stretchr/testify/assert"
)

func testPostTodoSuccess(t *testing.T, r *gin.Engine, reqData *schema.TodoCreateReq) *schema.TodoCommonResp {
	t.Log(reqData)
	body, _ := json.Marshal(reqData)
	req, _ := http.NewRequest("POST", "/api/todo", bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)

	bs := w.Body.Bytes()
	t.Log(string(bs))

	var resp *schema.TodoCommonResp
	assert.NoError(t, json.Unmarshal(bs, &resp))
	assert.Equal(t, reqData.Subject, resp.Subject)
	if reqData.Due == nil {
		assert.Nil(t, resp.Due)
	} else {
		assert.True(t, resp.Due.Equal(*reqData.Due))
	}
	assert.False(t, resp.Done)
	if diff := cmp.Diff(time.Now(), resp.CreatedAt, cmpopts.EquateApproxTime(100*time.Millisecond)); diff != "" {
		t.Errorf("[POST /api/todo] CreatedAt mistatch (-want +got)\n%s", diff)
	}
	assert.True(t, resp.CreatedAt.Equal(resp.UpdatedAt))

	return resp
}

func testGetTodoList(t *testing.T, r *gin.Engine, expected []schema.TodoCommonResp) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/todo", nil)
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var actual []schema.TodoCommonResp
	assert.NoError(t, json.Unmarshal(w.Body.Bytes(), &actual))

	if diff := cmp.Diff(expected, actual, cmpopts.EquateApproxTime(time.Microsecond)); diff != "" {
		t.Errorf("GET response mistatch (-want +got):\n%s", diff)
	}
}

func testPutTodoSuccess(
	t *testing.T,
	r *gin.Engine,
	id uint,
	reqData *schema.TodoUpdateReq,
	beforeUpdate schema.TodoCommonResp,
) *schema.TodoCommonResp {
	t.Log(reqData)
	body, _ := json.Marshal(reqData)
	req, _ := http.NewRequest("PUT", fmt.Sprintf("/api/todo/%d", id), bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	bs := w.Body.Bytes()
	t.Log(string(bs))

	var resp *schema.TodoCommonResp
	assert.NoError(t, json.Unmarshal(bs, &resp))
	assert.Equal(t, reqData.Subject, resp.Subject)
	assert.Equal(t, beforeUpdate.Done, resp.Done)
	if diff := cmp.Diff(reqData.Due, resp.Due, cmpopts.EquateApproxTime(time.Microsecond)); diff != "" {
		t.Errorf("Due mistatch (-want +got)\n%s", diff)
	}
	if diff := cmp.Diff(time.Now(), resp.UpdatedAt, cmpopts.EquateApproxTime(100*time.Millisecond)); diff != "" {
		t.Errorf("UpdatedAt mistatch (-want +got)\n%s", diff)
	}
	if diff := cmp.Diff(beforeUpdate.CreatedAt, resp.CreatedAt, cmpopts.EquateApproxTime(time.Microsecond)); diff != "" {
		t.Errorf("CreatedAt mistatch (-want +got)\n%s", diff)
	}

	return resp
}

func testPutTodoDoneSuccess(
	t *testing.T,
	r *gin.Engine,
	id uint,
	done bool,
	beforeUpdate schema.TodoCommonResp,
) *schema.TodoCommonResp {
	body := fmt.Sprintf(`{"done": %v}`, done)
	req, _ := http.NewRequest("PUT", fmt.Sprintf("/api/todo/%d/done", id), bytes.NewBufferString(body))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	t.Logf("id: %d,  body: %s", id, body)
	assert.Equal(t, http.StatusOK, w.Code)

	bs := w.Body.Bytes()
	t.Log(string(bs))

	var resp *schema.TodoCommonResp
	assert.NoError(t, json.Unmarshal(bs, &resp))
	assert.Equal(t, beforeUpdate.Subject, resp.Subject)
	assert.Equal(t, done, resp.Done)
	if diff := cmp.Diff(beforeUpdate.Due, resp.Due, cmpopts.EquateApproxTime(time.Microsecond)); diff != "" {
		t.Errorf("Due mistatch (-want +got)\n%s", diff)
	}
	if diff := cmp.Diff(time.Now(), resp.UpdatedAt, cmpopts.EquateApproxTime(100*time.Millisecond)); diff != "" {
		t.Errorf("UpdatedAt mistatch (-want +got)\n%s", diff)
	}
	if diff := cmp.Diff(beforeUpdate.CreatedAt, resp.CreatedAt, cmpopts.EquateApproxTime(time.Microsecond)); diff != "" {
		t.Errorf("CreatedAt mistatch (-want +got)\n%s", diff)
	}

	return resp
}

func TestTodoApi(t *testing.T) {
	DB := db.OpenOrDie(os.Getenv("DATABASE_TEST_URL"))
	db.MigrateAllOrDie(DB)
	defer db.DropAllOrDie(DB)

	router := gin.Default()
	controller.RegisterTodoAPIs(router, DB)

	var todo1, todo2, todo3 schema.TodoCommonResp

	t.Run("Post new todo with due (due is future)", func(t *testing.T) {
		due := time.Date(2024, 9, 28, 23, 59, 59, 0, time.Local)
		todo1 = *testPostTodoSuccess(t, router, &schema.TodoCreateReq{
			Subject: "todo1",
			Due:     &due,
		})
	})
	time.Sleep(300 * time.Millisecond)
	t.Run("Post new todo without due", func(t *testing.T) {
		todo2 = *testPostTodoSuccess(t, router, &schema.TodoCreateReq{
			Subject: "todo2",
			Due:     nil,
		})
	})
	time.Sleep(300 * time.Millisecond)
	t.Run("Post new todo with due (due is past)", func(t *testing.T) {
		due := time.Date(1990, 9, 28, 0, 0, 0, 0, time.UTC)
		todo3 = *testPostTodoSuccess(t, router, &schema.TodoCreateReq{
			Subject: "todo3",
			Due:     &due,
		})
	})

	t.Log("after created 3 todos: ", todo1, todo2, todo3)

	t.Run("Get todo list: should be ascending order of due & NULL of due comes last", func(t *testing.T) {
		testGetTodoList(t, router, []schema.TodoCommonResp{todo3, todo1, todo2})
	})
	t.Run("Update todo2", func(t *testing.T) {
		due := time.Date(2019, 3, 20, 12, 0, 59, 0, time.UTC)
		todo2 = *testPutTodoSuccess(t, router, todo2.ID, &schema.TodoUpdateReq{
			Subject: "**updated**",
			Due:     &due,
		}, todo2)
	})
	t.Run("Make todo1 done", func(t *testing.T) {
		todo1 = *testPutTodoDoneSuccess(t, router, todo1.ID, true, todo1)
	})
	t.Run("Delete todo3", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/api/todo/%d", todo3.ID), nil)
		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)
	})

	t.Run("Get todo list: must be applied PATCH & DELETE", func(t *testing.T) {
		testGetTodoList(t, router, []schema.TodoCommonResp{todo2, todo1})
	})
}
