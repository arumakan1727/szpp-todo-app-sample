-- 開発時の通常DB。
CREATE DATABASE myapp_dev;

-- 単体テスト等で使用。
CREATE DATABASE myapp_test;
