package main

import (
	"log"
	"os"

	"github.com/arumakan1727/todo-app-pre/back-end/cfg"
	"github.com/arumakan1727/todo-app-pre/back-end/controller"
	"github.com/arumakan1727/todo-app-pre/back-end/db"
	"github.com/gin-gonic/gin"
)

func main() {
	DB := db.OpenOrDie(os.Getenv("DATABASE_URL"))
	db.MigrateAllOrDie(DB)

	router := gin.Default()
	router.Use(cfg.NewCorsConfig())
	controller.RegisterTodoAPIs(router, DB)

	port := cfg.GetEnvOrDefault("PORT", "8080")
	log.Fatal(router.Run(":" + port))
}
