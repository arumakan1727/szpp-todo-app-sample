package db

import (
	"database/sql"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"github.com/arumakan1727/todo-app-pre/back-end/cfg"
	"github.com/arumakan1727/todo-app-pre/back-end/model"
	_ "github.com/lib/pq"
)

func OpenOrDie(dbUrl string) *gorm.DB {
	if dbUrl == "" {
		log.Fatalln("[Error] dbUrl is empty.")
	}

	sqlDB, err := sql.Open("postgres", dbUrl)
	if err != nil {
		log.Println("[Error] Failed to open postgres DB; dbUrl: ", dbUrl)
		log.Fatalln(err)
	}

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{
		Logger: cfg.NewCustomGormLogger(os.Stderr, cfg.IsDebugMode()),
	})
	if err != nil {
		log.Println("[Error] Failed to create gorm DB.")
		log.Fatalln(err)
	}

	return gormDB
}

func MigrateAllOrDie(db *gorm.DB) {
	err := db.AutoMigrate(
		&model.Todo{},
		&model.DeletedTodo{},
	)
	if err != nil {
		log.Fatalln("[Error] Failed to migrate: ", err)
	}
	log.Println("[Info] Successfully migrated all")
}

func DropAllOrDie(db *gorm.DB) {
	err := db.Migrator().DropTable(
		&model.Todo{},
		&model.DeletedTodo{},
	)
	if err != nil {
		log.Fatalln("[Error] Failed to drop table: ", err)
	}
	log.Println("[Info] Successfully dropped all table")
}
