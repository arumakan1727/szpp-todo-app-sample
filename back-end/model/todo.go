// DBテーブルのモデル定義。gorm ライブラリと併用する。
package model

import (
	"fmt"
	"time"
)

type Todo struct {
	ID        uint      `gorm:"primaryKey"`
	Subject   string    `gorm:"not null;size:255"`
	Due       time.Time `gorm:"not null"`
	Done      bool      `gorm:"not null;default:false"`
	CreatedAt time.Time `gorm:"not null"`
	UpdatedAt time.Time `gorm:"not null"`
}

type DeletedTodo struct {
	Todo      `gorm:"embedded"`
	DeletedAt time.Time `json:"deleted_at" gorm:"not null"`
}

// とても未来の日付。
// ORDER BY due をしやすくするために NULL 値ではなく farFutureDate で NULL を表現する。
var farFutureDate time.Time

func init() {
	farFutureDate = time.Date(9999, 12, 31, 23, 59, 59, 0, time.UTC)
}

func GetNullDue() time.Time {
	return farFutureDate
}

func ConvertDueIfNil(due *time.Time) time.Time {
	if due == nil {
		return GetNullDue()
	}
	return *due
}

func (t Todo) HasDue() bool {
	return !t.Due.Equal(GetNullDue())
}

func (t Todo) MaybeDue() *time.Time {
	if t.HasDue() {
		return &t.Due
	}
	return nil
}

func (t Todo) String() string {
	return fmt.Sprintf("Todo{ID: %d, Subject: %q, Due: %s, Done: %v, CreatedAt: %s, UpdatedAt: %s}",
		t.ID,
		t.Subject,
		t.Due.Format(time.RFC3339),
		t.Done,
		t.CreatedAt.Format(time.RFC3339),
		t.UpdatedAt.Format(time.RFC3339),
	)
}

func (t DeletedTodo) String() string {
	return fmt.Sprintf("Todo{ID: %d, Subject: %q, Due: %s, Done: %v, CreatedAt: %s, UpdatedAt: %s, DeletedAt: %s}",
		t.ID,
		t.Subject,
		t.Due.Format(time.RFC3339),
		t.Done,
		t.CreatedAt.Format(time.RFC3339),
		t.UpdatedAt.Format(time.RFC3339),
		t.DeletedAt.Format(time.RFC3339),
	)
}
