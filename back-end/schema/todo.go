// APIのリクエストやレスポンスで用いるJSONのスキーマ定義。
package schema

import (
	"fmt"
	"time"
)

type TodoCreateReq struct {
	Subject string     `json:"subject" binding:"required,max=255"`
	Due     *time.Time `json:"due"`
}

type TodoCommonResp struct {
	ID        uint       `json:"id"`
	Subject   string     `json:"subject"`
	Due       *time.Time `json:"due"`
	Done      bool       `json:"done"`
	CreatedAt time.Time  `json:"crated_at"`
	UpdatedAt time.Time  `json:"updated_at"`
}

func formatTime(t *time.Time) string {
	if t == nil {
		return "<nil>"
	}
	return t.Format(time.RFC3339)
}

func (t TodoCommonResp) String() string {

	return fmt.Sprintf(
		`Todo{
  ID: %d,
  Subject: %q,
  Due: %s,
  Done: %v,
  CreatedAt: %s,
  UpdatedAt: %s
}`,
		t.ID,
		t.Subject,
		formatTime(t.Due),
		t.Done,
		formatTime(&t.CreatedAt),
		formatTime(&t.UpdatedAt),
	)
}

type TodoUpdateReq = TodoCreateReq

type TodoDoneUpdateReq struct {
	Done bool `json:"done" binding:"required"`
}
