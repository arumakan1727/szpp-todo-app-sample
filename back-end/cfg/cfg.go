package cfg

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm/logger"
)

var debugMode bool

func init() {
	mode := os.Getenv("APP_DEBUG_MODE")
	debugMode = (mode == "1" || mode == "true" || mode == "yes" || mode == "on")
	log.Println("[Info] $APP_DEBUG_MODE: ", mode, ", so IsDebugMode(): ", IsDebugMode())
}

func IsDebugMode() bool {
	return debugMode
}

func GetEnvOrDefault(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func NewCustomGormLogger(out *os.File, isDebug bool) logger.Interface {
	logLevel := logger.Warn
	if isDebug {
		logLevel = logger.Info
	}

	return logger.New(
		log.New(out, "\n", log.LstdFlags),
		logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logLevel,
			IgnoreRecordNotFoundError: false,
			Colorful:                  true,
		},
	)
}

func getAllowOrigins() []string {
	allowOrigins := make([]string, 0, 3)

	for i := 1; i <= 3; i++ {
		origin := os.Getenv(fmt.Sprintf("ALLOW_ORIGIN_%d", i))
		if origin != "" {
			allowOrigins = append(allowOrigins, origin)
		}
	}
	return allowOrigins
}

func NewCorsConfig() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowOrigins:     getAllowOrigins(),
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
	})
}
