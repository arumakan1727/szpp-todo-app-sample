export default interface TodoItemModel {
  id: number;
  subject: string;
  due: Date | null;
  done: boolean;
}
